# Kamil BarcodeUtils


## Getting started
It has simple --help message, it's pretty straighforward from here.

## Description
A collection of python function for dealing with barcodes in PDFs. NOT production ready by any means, use at your own risk.

## Installation
TODO

## Support and warranty
Absolutely none.

## Roadmap
-Adding barcodes into files.
-Handling of non standard aspect ratios.

## Contributing
Feel free to suggest any changes/fixes. Examples of PDFs not working as expected also welcome.

## License
Standard AGPL, may change in the future.
