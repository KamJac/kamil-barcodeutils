import argparse
import pikepdf
import asyncio
import sys
import pyzbar.pyzbar
import json
import os
import barcode

parser = argparse.ArgumentParser(description = "A simple collection of python functions for managing pdfs and barcodes on them. Checking arguments is implemented very -ish, so think about what you wanna do.")

parser.add_argument('-o', '--output', help = 'File to output into')
parser.add_argument('-f', '--file', help = 'File to process')
parser.add_argument('-i', '--inplace', help = 'Change file in place (applies only to some actions)', action='store_true')
parser.add_argument('-a', '--action', help = 'Action to do', required=True, choices = ['extract_barcodes_into_json', 'normalize_file', 'place_barcode'])
parser.add_argument('-v', '--verbose', help = 'Print more info', action='store_true')
parser.add_argument('-b', '--barcode', help = 'Barcode to process')
parser.add_argument('--cake', action='store_true')

args = parser.parse_args()

async def get_barcode_from_page(page, code_type='CODE39'):
    """
    Tries to read barcode from all the images in a one page.

    :param page: One pae of read pdf file.
    :type page: pikepdf.Page
    :param code_type: A type pf barcode to read.
    :type code_type: str
    :return: A read barcode or empty string.
    :rtype: str
    """
    if (args.verbose): print(f'get_barcode_from_page Start.')
    if(len(page.images.keys()) < 1):
        print('get_barcode_from_page Page has less than 1 image!', file=sys.stderr)
        return None
    else:
        for image_name in page.images.keys():
            if (args.verbose): print(f'get_barcode_from_page Getting barcode from image {str(image_name)}.')
            try:
                image = pikepdf.PdfImage(page.images[image_name]).as_pil_image()
            except:
                print(f'get_barcode_from_page Cannot extract image {str(image_name)}!', file=sys.stderr)
                return None
            try:
                barcode = str(pyzbar.pyzbar.decode(image, symbols=[pyzbar.pyzbar.ZBarSymbol[code_type]])[0].data).split("'")[1]
            except IndexError:
                print(f'get_barcode_from_page Cannot extract barcode {str(image_name)}!', file=sys.stderr)
                barcode = ''
        if (args.verbose): print(f'get_barcode_from_page End.')
        return barcode


async def generate_barcodes_list(pdf, filename):
    """
    Iterates over all pages, reads the barcode from each and constructs a list of barcodes on pages.

    :param pdf: A handle object to opened pdf file.
    :type pdf: pikepdf._qpdf.Pdf
    :param filename: A name of the opened file.
    :type filename: str
    :return: Each entry is dict with keys FileName, PageNo, Barcode.
    :rtype: list
    """

    if(args.verbose): print(f'generate_barcodes_list Start {str(filename)}.')
    result = list()
    if(len(pdf.pages) < 1):
        print(f'generate_barcodes_list Pdf {str(filename)} has less than 1 page!', file=sys.stderr)
        return None
    else:
        for page_index in range(0, len(pdf.pages) ):
            if(args.verbose): print(f'generate_barcodes_list Processing page index {str(page_index)}.')
            page_barcode = asyncio.create_task(get_barcode_from_page(pdf.pages[page_index]))
            page_result = dict()
            page_result['FileName'] = os.path.basename(filename)
            page_result['PageNo'] = page_index+1
            page_result['Barcode'] = await page_barcode
            result.append(page_result)
    if(args.verbose): print(f'generate_barcodes_list End.')
    return result


async def open_file(path):
    """
    Tries to open and parse filee under given path.

    :param path: Path to file to be opened.e
    :type path: str
    :return: Handle object to opened file.
    :rtype: pikepdf._qpdf.Pdf
    """

    if(args.verbose): print(f'open_file Start {str(path)}.')
    try:
        result = pikepdf.Pdf.open(path)
    except FileNotFoundError:
        print(f'open_file Cannot find file {str(path)}!', file=sys.stderr)
        return None
    except TypeError:
        print(f'open_file Wrong path {str(path)}!', file=sys.stderr)
        return None
    except pikepdf.PasswordError:
        print(f'open_file Wrong decryption password for {str(path)}!', file=sys.stderr)
        return None
    except:
        print(f'open_file Cannot open file {str(path)}!', file=sys.stderr)
        return None
    if(args.verbose): print(f'open_file End.')
    return result


async def extract_barcodes_into_json():
    """
    Loads the file, reads barcodes and dumps them into json.

    :return: Status code.
    :rtype: int
    """

    if (args.verbose): print(f'extract_barcodes_into_json Start.')
    pdf_file = asyncio.create_task(open_file(args.file) )
    if (type(await pdf_file) == pikepdf._qpdf.Pdf):
        barcodes_list = asyncio.create_task(generate_barcodes_list(await pdf_file, args.file))
        if type(await barcodes_list) is list:
            if (args.verbose): print(f'extract_barcodes_into_json Saving to file.')
            try:
                with open(args.output, 'w') as output_file:
                    json.dump(await barcodes_list, output_file)
            except:
                print(f'extract_barcodes_into_json Cannot save to file {str(args.output)}!', file=sys.stderr)
                return -1
        else:
            print(f'extract_barcodes_into_json Cannot generate proper dict!', file=sys.stderr)
            return -1
        return 0
    else:
        print('extract_barcodes_into_json Invalid pdf!', file=sys.stderr)
        return -1


async def normalize_file():
    """
    Manipulates a file, making it easier to read.
    Mainly, rasterizes, copressess images, decopresses sructure, changes version to 1.4 etc.

    :return: Status code.
    :rtype: int
    """

    if (args.verbose): print(f'normalize_file Start.')
    pdf_file = await open_file(args.file)
    if (type(pdf_file) == pikepdf._qpdf.Pdf):
        pdf_file.save(args.output, force_version='1.4' )
        print(pdf_file.pdf_version)
    else:
        print('normalize_file Invalid pdf!', file=sys.stderr)
        return -1
    if (args.verbose): print(f'normalize_file End.')


async def place_barcode():
    """
    Places barcode on pdf.

    :return: Status code.
    :rtype: int
    """

    if (args.verbose): print(f'place_barcode Start.')
    pdf_file = asyncio.create_task(open_file(args.file))
    if (type(await pdf_file) == pikepdf._qpdf.Pdf):
        code = asyncio.create_task(generate_barcode_image(args.barcode))
    else:
        print('extract_barcodes_into_json Invalid pdf!', file=sys.stderr)
        return -1
    if (args.verbose): print(f'place_barcode End.')


async def generate_barcode_image(barcode_text, code_type='CODE39'):
    """
    Generates a barcode image of given type, encoded with given information.

    :param barcode_text: Information to be encoded into barcode.
    :type barcode_text: str
    :param code_type: Type of barcode to be generated.
    :type code_type: str
    :return: Generated image.
    :rtype: pillow image #TODO proper object type
    """

    if (args.verbose): print(f'generate_barcode_image Start.')
    #barcode_obj = barcode.Code39(barcode_text, barcode.writer.ImageWriter(), False)
    barcode_obj = barcode.get(code_type, barcode_text, barcode.writer.ImageWriter(), False)
    barcode_pil = barcode_obj.render({'module_width': 1, 'module_height': 12, 'font_size': 28})
    if (args.verbose): print(f'generate_barcode_image Start.')
    return barcode_pil
#TODO some checks and exception handling

async def main():
	if(args.cake): print("Dział IDR could never!")
	if(args.action == 'extract_barcodes_into_json'):
		if args.output is None:
			print('main Missing required argument "output"!', file=sys.stderr)
			return -1
		if args.file is None:
			print('main Missing required argument "file"!', file=sys.stderr)
			return -1
		return await extract_barcodes_into_json()
	elif(args.action == 'normalize_file'):
		if args.file is None:
			print('main Missing required argument "file"!', file=sys.stderr)
			return -1
		if (args.inplace is None) and (args.output is None):
			print('main Missing required argument "output" or "inplace"!', file=sys.stderr)
			return -1
		if (args.output is None) and (args.inplace is not None):
			args.output = args.file
		return await normalize_file()
	elif(args.action == 'place_barcode'):
		if args.output is None:
			print('main Missing required argument "output"!', file=sys.stderr)
			return -1
		if args.file is None:
			print('main Missing required argument "file"!', file=sys.stderr)
			return -1
		if args.barcode is None:
			print('main Missing required argument "barcode"!', file=sys.stderr)
			return -1
		return await place_barcode()
	else:
		if(args.verbose): print(f'main {str(args.action)}')
		return

asyncio.run(main() )
